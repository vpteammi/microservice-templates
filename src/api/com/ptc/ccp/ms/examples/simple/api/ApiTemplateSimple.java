/*
 History:
  Created by vparfenov on 01-12-2018.
 */

package com.ptc.ccp.ms.examples.simple.api;

import java.lang.reflect.Type;

import com.google.common.reflect.TypeToken;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.ServiceMessage;

import io.reactivex.Single;

public class ApiTemplateSimple extends ReactiveClient<ApiTemplateSimple>
{
	public ApiTemplateSimple (IReactiveConnector connection)
	{
		super (connection);
	}

	public static class Message implements JsonSerializable
	{
		public String text;

		public Message (String text)
		{
			super ();
			this.text = text;
		}

		@SuppressWarnings ("serial")
		public static final Type TYPE = new TypeToken<ServiceMessage<Message>> ()
		{
		}.getType ();
	}

	public Single<Message> ask (Message msg)
	{
		return sendRx ("ask", msg, Message.TYPE);
	}
}
