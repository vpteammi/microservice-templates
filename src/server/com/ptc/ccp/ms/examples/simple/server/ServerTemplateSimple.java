/*
 History:
  Created by vparfenov on 01-12-2018.
 */

package com.ptc.ccp.ms.examples.simple.server;

import com.ptc.ccp.ms.examples.simple.api.ApiTemplateSimple;
import com.ptc.ccp.ms.examples.simple.api.ApiTemplateSimple.Message;
import com.ptc.microservices.reactive.IReactiveFramework;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.microservices.reactive.ServiceRequest;

public class ServerTemplateSimple extends ReactiveService
{
	public static void main (String[] args)
	{
		IReactiveFramework.getDefault ().createServer (args)
				.registerEventBusService (ApiTemplateSimple.class, new ReactiveService ()
				{
					{
						registerMethod ("ask",
								(ServiceRequest<Message, Message> request) -> request.respond (
										new Message ("Hello " + request.data ().text + "! ServerTemplateSimple")),
								Message.TYPE);
					}
				}).start ();
	}
}
