/*
 History:
  Created by vparfenov on 01-12-2018.
 */

package com.ptc.ccp.ms.examples.simple.client;

import com.ptc.ccp.ms.examples.simple.api.ApiTemplateSimple;
import com.ptc.ccp.ms.examples.simple.api.ApiTemplateSimple.Message;
import com.ptc.microservices.reactive.IReactiveFramework;

public class ClientTemplateSimple
{
	public static void main (String[] args)
	{
		IReactiveFramework reactiveFramework = IReactiveFramework.getDefault ();
		reactiveFramework.connectToServerRx (ApiTemplateSimple.class)
				.flatMap (api -> api.ask (new Message ("ClientTemplateSimple")))
				.doOnError (failure -> System.err.println (failure)).doFinally ( () -> reactiveFramework.shutdown ())
				.subscribe (message -> System.out.println ("Response: " + message.text));
		System.out.println ("ClientTemplateSimple started");
	}
}
